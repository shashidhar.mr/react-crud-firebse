import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import LoginContext from "../context/loginContext";

export default function Login() {
  const context = useContext(LoginContext);
  const history = useHistory();
  const [username1, setUsername1] = useState("");

  const [password1, setPassword1] = useState("");

  const login = (event) => {
    const data = localStorage.getItem("data");

    console.log(data);

    let reqobj = JSON.parse(data);
    console.log("reqobj", reqobj);
    console.log(reqobj.username);
    console.log(username1);

    if (reqobj.username === username1 && reqobj.password === password1) {
      alert("User Login is completed");
      context.changeLogin(true);

      history.push("/Register");
    } else {
      alert("Invalid username and password");
    }
    event.preventDefault();
  };
  const register = () => {
    history.push("/Reg1");
  };

  return (
    <form onSubmit={login}>
      <div>
        <div className="col-md-4 offset-md-4 mt-5">
          <div className="card">
            <div className="card-header text-center text-white bg-secondary">
              <h3>Login to Account</h3>
            </div>
            <div className="card-body">
              <div className="form-group">
                <label for="email">Username *</label>
                <input
                  id="email"
                  type="text"
                  name="username1"
                  value={username1}
                  required
                  onChange={(e) => setUsername1(e.target.value)}
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label for="password">Password *</label>
                <input
                  type="password"
                  id="password"
                  type="text"
                  value={password1}
                  required
                  onChange={(e) => setPassword1(e.target.value)}
                  className="form-control"
                  required
                />
              </div>
            </div>
            <div className="card-footer">
              <button
                className="btn btn-secondary float-right"
                onClick={login}
                style={{ marginLeft: "650px" }}
              >
                Login
              </button>
              <button
                className="btn btn-secondary "
                onClick={register}
                style={{ marginLeft: "0px" ,marginTop:"-60px"}}
              >
                Register
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}
